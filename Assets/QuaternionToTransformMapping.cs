﻿using Assets.LSL4Unity.Scripts.AbstractInlets;
using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class QuaternionToTransformMapping : AFloatInlet
{
    public Transform targetTransform;

    //Import the following.
    [DllImport("user32.dll", EntryPoint = "SetWindowText")]
    public static extern bool SetWindowText(System.IntPtr hwnd, System.String lpString);
    [DllImport("user32.dll", EntryPoint = "FindWindow")]
    public static extern System.IntPtr FindWindow(System.String className, System.String windowName);

    private string currentWindowTitle;

    protected void OnEnable()
    {
        currentWindowTitle = Application.productName;

        Application.targetFrameRate = 15;

        string[] args = Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if ((args[i] == "-p" || args[i] == "--predicate") && i + 1 < args.Length)
            {
                StreamPredicate = args[i + 1];
                Console.WriteLine("Looking for LSL predicate >>>" + StreamPredicate + "<<< ...");
                setWindowTitle(StreamPredicate);
                return;
            }
        }
        Application.Quit(1);
    }

    protected override void Process(float[] newSample, double timeStamp)
    {
        if (currentWindowTitle == StreamPredicate && inlet != null)
        {
            setWindowTitle(inlet.info().name());
        }

        float x = newSample[1];
        float y = newSample[2];
        float z = newSample[3];
        float w = newSample[4];

        // we map the coordinates to a rotation
        var targetRotation = new Quaternion(x, y, z, w);

        // apply the rotation to the target transform
        targetTransform.rotation = targetRotation;
    }

    private void setWindowTitle(string newTitle)
    {
        //Get the window handle.
        var windowPtr = FindWindow(null, currentWindowTitle);
        //Set the title text using the window handle.
        SetWindowText(windowPtr, newTitle);

        currentWindowTitle = newTitle;
    }
}
