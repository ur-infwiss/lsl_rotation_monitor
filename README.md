LSL Rotation Monitor
====================
This is a Unity 3D project that displays an LSL stream that contains Quaternions. Can be used to display the current orientation of an IMU (combined accelerometer and magnetometer sensor).